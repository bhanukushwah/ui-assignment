import axios from "axios";
import { IUserCardProps } from "../components/UserCard/IUserCardProps";

const getUser = async (): Promise<IUserCardProps[]> => {
  try {
    const { data } = await axios.get("https://reqres.in/api/users/");
    return data.data;
  } catch (error) {
    throw new Error(error);
  }
};

export default getUser;
