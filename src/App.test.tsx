import React from "react";
import { shallow } from "enzyme";
import App from "./App";

describe("App is testing", () => {
  it("App component is rendering without any crashes", () => {
    const wrapper = shallow(<App />);
    expect(wrapper).toBeTruthy();
  });
});
