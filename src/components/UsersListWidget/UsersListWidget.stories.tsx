import React, { ComponentProps } from "react";
import { Story } from "@storybook/react";

import UsersListWidget from "./UsersListWidget";

export default {
  title: "UsersList",
  component: UsersListWidget,
  args: {
    title: "Users",
    users: [
      {
        id: 1,
        first_name: "George",
        last_name: "Bluth",
        email: "geo@gmail.com",
        avatar: "https://reqres.in/img/faces/1-image.jpg",
      },
    ],
  },
};

const Template: Story<ComponentProps<typeof UsersListWidget>> = (args) => (
  <UsersListWidget {...args} />
);

export const UsersList = Template.bind({});
UsersList.args = {
  title: "Users",
  users: [
    {
      id: 1,
      first_name: "George",
      last_name: "Bluth",
      email: "geo@gmail.com",
      avatar: "https://reqres.in/img/faces/1-image.jpg",
    },
    {
      id: 2,
      first_name: "George",
      last_name: "Bluth",
      email: "geo@gmail.com",
      avatar: "https://reqres.in/img/faces/1-image.jpg",
    },
  ],
};
