export interface IUserCardProps {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}
