import React, { FC } from "react";

import "./Header.scss";

const Header: FC = () => {
  return (
    <nav className="nav-container">
      <div className="container navbar">
        <div className="logo">Logo</div>

        <ul className="nav-items">
          <li className="nav-item">Users</li>
        </ul>
      </div>
    </nav>
  );
};

export default Header;
